//el endpoint para obtener la póliza, podria ser leido desde una configuración global
const POLICY_API =
  "https://dn8mlk7hdujby.cloudfront.net/interview/insurance/policy";

//reglas de precio definidas para la póliza, deberia ser leida desde una BD o configuración global
const PRICE_RULES = {
  LIFE: {
    0: 0.279,
    1: 0.4396,
    2: 0.5599
  },
  DENTAL: {
    0: 0.12,
    1: 0.195,
    2: 0.248
  }
};

//edad máxima donde la póliza tiene cobertura, debería ser leida desde una configuración global
const MAX_AGE = 65;

module.exports = {
  POLICY_API,
  PRICE_RULES,
  MAX_AGE
};
