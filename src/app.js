const express = require("express");
const { calculateWorkersPolicyCost, getPolicyData } = require("./utils");

const app = express();

app.get("/", async function(req, res) {
  try {
    //se llama al endpoint con la información de la póliza y el listado de trabajadores
    const response = await getPolicyData();
    const data = await response.json();
    const policy = calculateWorkersPolicyCost(data);
    res.send(policy);
  } catch (error) {
    //si ocurriera cualquier error en el proceso, será capturado aquí y devuelto al cliente
    res
      .status(500)
      .send({ msg: "Error al consumir la API de póliza", error: error });
  }
});

module.exports = app;
