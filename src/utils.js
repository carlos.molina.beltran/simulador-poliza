const fetch = require("node-fetch");
const { PRICE_RULES, MAX_AGE, POLICY_API } = require("./constants");

/**
 * Obtiene la información de la póliza y los trabajadores desde la API externa
 */
const getPolicyData = async () => {
  return await fetch(POLICY_API);
};

/**
 * Calcula los costos de la póliza para cada trabajador
 * @param {Object} data el objeto con la información de la API
 */
const calculateWorkersPolicyCost = data => {
  const { policy } = data;
  //se verifica que la información del endpoint contenga la póliza
  if (policy) {
    //se verifica que la póliza contenga un listado de trabajadores válido
    if (Array.isArray(policy.workers) && policy.workers.length > 0) {
      const { has_dental_care, company_percentage } = policy;
      //se recorre el listado de trabajadores para calcular la póliza para cada uno
      const workers = policy.workers.map(worker =>
        policyCost(worker, has_dental_care, company_percentage)
      );
      //se entrega el listado de trabajadores al cliente
      return workers;
    } else {
      throw "No se obtuvo un listado de trabajadores para calcular la póliza";
    }
  } else {
    throw "No se obtuvo información de la póliza";
  }
};

/**
 * La función permite obtener el costo de la póliza para un trabajador
 * @param {Object} worker el objeto con la información del trabajador
 * @param {boolean} has_dental_care indica si se incluye cobertura dental en la póliza
 * @param {number} company_percentage indica el porcentaje de la póliza que es cubierto por la empresa
 */
const policyCost = (worker, has_dental_care, company_percentage) => {
  //se valida que el objeto worker contenga valores
  if (worker) {
    //se extraen desde worker los atributos necesarios para calcular la póliza
    const { age, childs } = worker;
    //se verifica que age y childs sean números validos y >= 0
    if (!isNaN(age) && !isNaN(childs) && age >= 0 && childs >= 0) {
      //se válida que la edad del trabajador le permita acceder a la póliza
      if (age <= MAX_AGE) {
        //se declaran las variables para costos
        let lifeCost, dentalCost;
        //se identifica la regla que se usará en el cálculo
        let rule = childs < 2 ? childs : 2;
        //se calcula el costo del seguro de vida
        lifeCost = PRICE_RULES.LIFE[rule];
        //se calcula el costo del seguro dental, si corresponde
        dentalCost = has_dental_care ? PRICE_RULES.DENTAL[rule] : 0;
        //se retorna el objeto que contiene la respuesta de la función
        return {
          ...worker,
          is_valid: true,
          data: makePolicyData(lifeCost, dentalCost, company_percentage), //se llama a la funcion que construye el objeto con los datos del costo
          error: null
        };
      } else {
        //en este caso se retorna on objeto detallando el error MAX_AGE, sin datos del calculo
        return {
          ...worker,
          is_valid: false,
          data: null,
          error: `worker's age exceeds the maximum: worker: ${age}, max: ${MAX_AGE}`
        };
      }
    } else {
      //en este caso se retorna on objeto detallando el error en los atributos del empleado, sin datos del calculo
      return {
        ...worker,
        is_valid: false,
        data: null,
        error: `worker data is invalid: age: ${age}, childs: ${childs}`
      };
    }
  } else {
    //en este caso se retorna on objeto detallando el error en el objeto de empleado, sin datos del calculo
    return {
      is_valid: false,
      data: null,
      error: `worker is invalid: worker: ${worker}`
    };
  }
};

/**
 * La función construye el objeto con los costos de la póliza
 * @param {number} lifeCost indica el costo del seguro de vida
 * @param {number} dentalCost indica el costo del seguro dental
 * @param {number} companyPercentage indica el porcentaje de cobertura de la empresa
 */
const makePolicyData = (lifeCost, dentalCost, companyPercentage) => {
  //se calcula el costo total de la póliza
  let totalCost = lifeCost + dentalCost;
  //se calcula el monto que cubre la empresa
  let companyCost = parseFloat(
    ((totalCost * companyPercentage) / 100).toFixed(4)
  );
  //se calcula el costo que debe cubrir el empleado
  let employeeCost = parseFloat((totalCost - companyCost).toFixed(4));
  //se retorna el objeto con los datos del costo
  return {
    total_cost: totalCost, //costo total de la poliza, con dental si corresponde
    company_cost: companyCost, //el monto cubierto por la empresa
    employee_cost: employeeCost, // el copago del trabajador
    detail: {
      // detalle del costo de póliza por categoria, sin separar empresa y trabajador, la suma de ellos debe ser igual a total_cost
      life: lifeCost,
      dental: dentalCost
    }
  };
};

module.exports = { getPolicyData, policyCost, calculateWorkersPolicyCost };
