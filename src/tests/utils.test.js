const { policyCost, calculateWorkersPolicyCost } = require("../utils");

describe("Test policyCost function", () => {
  //se valida que con un trabajador válido con un hijo, la función calcula una póliza válida
  test("It should valid policy", async () => {
    const worker = { age: 32, childs: 1 };
    const cost = policyCost(worker, false, 100);
    expect(cost.is_valid).toBe(true);
  });

  //se valida que con un trabajador válido la función calcula una póliza válida, para el caso en que se incluye el seguro dental
  test("It should valid policy", async () => {
    const worker = { age: 32, childs: 1 };
    const cost = policyCost(worker, true, 100);
    expect(cost.is_valid).toBe(true);
  });

  //se valida que con un trabajador válido con 2 hijos, la función calcula una póliza válida
  test("It should valid policy", async () => {
    const worker = { age: 32, childs: 2 };
    const cost = policyCost(worker, false, 100);
    expect(cost.is_valid).toBe(true);
  });

  //se valida que con un trabajador válido con 2 hijos la función calcula una póliza válida, para el caso en que se incluye el seguro dental
  test("It should valid policy", async () => {
    const worker = { age: 32, childs: 2 };
    const cost = policyCost(worker, true, 100);
    expect(cost.is_valid).toBe(true);
  });

  //se valida que para un trabajador con una edad mayor a la máxima, no se calcule un póliza válida
  test("It should invalid policy", async () => {
    const worker = { age: 66, childs: 1 };
    const cost = policyCost(worker, false, 100);
    expect(cost.is_valid).toBe(false);
  });

  //se valida que para un objeto inválido para el trabajador, la póliza sea inválida
  test("It should invalid policy", async () => {
    const worker = {};
    const cost = policyCost(worker, false, 100);
    expect(cost.is_valid).toBe(false);
  });

  //se valida que para un objeto null para el trabajador, la póliza sea inválida
  test("It should invalid policy", async () => {
    const worker = null;
    const cost = policyCost(worker, false, 100);
    expect(cost.is_valid).toBe(false);
  });

  //se valida que para un trabajador válido con 1 hijo, se calcule la póliza con los montos correctos
  test("It should valid cost", async () => {
    const worker = { age: 32, childs: 1 };
    const cost = policyCost(worker, true, 100);
    expect(cost.data).toEqual({
      total_cost: 0.6346,
      company_cost: 0.6346,
      employee_cost: 0,
      detail: {
        life: 0.4396,
        dental: 0.195
      }
    });
  });

  //se valida que para un trabajador válido con 2 o más hijos, se calcule la póliza con los montos correctos
  test("It should valid cost", async () => {
    const worker = { age: 63, childs: 3 };
    const cost = policyCost(worker, false, 90);
    expect(cost.data).toEqual({
      total_cost: 0.5599,
      company_cost: 0.5039,
      employee_cost: 0.056,
      detail: {
        life: 0.5599,
        dental: 0
      }
    });
  });

  //se valida que para un trabajador válido, el costo total calculado, sea igual a la suma del costo del seguro de vida mas el seguro dental
  test("totalCost It should equal to lifeCost and dentalCost sum", async () => {
    const worker = { age: 32, childs: 1 };
    const cost = policyCost(worker, true, 100);
    const totalCost = cost.data.detail.life + cost.data.detail.dental;
    expect(cost.data.total_cost).toEqual(totalCost);
  });

  //se valida que ante un objeto de póliza nulo (aunque la respuesta de la API externa se válida), se lance el error indicado
  test("It should be a policy error ", async () => {
    try {
      calculateWorkersPolicyCost({ policy: null });
    } catch (error) {
      expect(error).toEqual("No se obtuvo información de la póliza");
    }
  });

  //se valida que ante un array workers nulo (aunque la respuesta de la API externa se válida), se lance el mensaje de error indicado
  test("It should be a workers array error ", async () => {
    try {
      calculateWorkersPolicyCost({ policy: { workers: null } });
    } catch (error) {
      expect(error).toEqual(
        "No se obtuvo un listado de trabajadores para calcular la póliza"
      );
    }
  });
});
