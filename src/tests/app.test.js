const request = require("supertest");
const app = require("../app");

describe("Test the root path", () => {
  //test para validar que una petición a / funcione correctamente
  test("It should response the GET method", async () => {
    const response = await request(app).get("/");
    //status 200 es señal de una respuesta correcta
    expect(response.statusCode).toBe(200);
  });
});
