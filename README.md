# Simulador de pólizas

El simulador de pólizas es un proyecto construido con [Express](https://expressjs.com/es/), pero que además puede ser ejecutado como una función utilizando [Serverless](https://serverless.com/).  
Para ejecutar un endpoint express como función local se utilizó [Serverless Offline Express](https://github.com/web-seven/serverless-offline-express)

# ¿Cómo probar?

**Ojo:** para ejecutar como función debemos tener serverless instalado: [Instrucciones](https://serverless.com/framework/docs/getting-started/)  
_Cuando se instala, debemos abrir otra terminal para ejecutar el proyecto, ya que la actual no tiene conocimiento del nuevo comando disponible_

- Clonar el repositorio
- Instalar las dependencias con `npm i`
- Usar, para esto existen las siguientes opciones
- `npm start`: lanzar la aplicación como un servidor express
- `npm test`: lanzar los test para la la versión express
- `npm run dev`: lanzar la versión de express para desarrollo, con hot loader
- `npm run serverless`: lanzar la versión como función serverless
